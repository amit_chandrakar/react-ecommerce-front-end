import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import SEO from "../../components/seo";
import { getDiscountPrice } from "../../helpers/product";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import {
    addToCartAsync,
    deleteFromCartAsync,
    decreaseQuantityAsync,
} from "../../store/slices/cart-slice";
import { cartItemStock } from "../../helpers/product";

const Cart = () => {
    let cartTotalPrice = 0;

    const [quantityCount] = useState(1);
    const dispatch = useDispatch();
    let { pathname } = useLocation();

    const currency = useSelector((state) => state.currency);
    const { cartItems } = useSelector((state) => state.cart);

    return (
        <>
            <SEO titleTemplate="Cart" description="Cart Details" />

            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Cart",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />
                <div className="cart-main-area pt-90 pb-100">
                    <div className="container">
                        {cartItems && cartItems.length >= 1 ? (
                            <>
                                <h3 className="cart-page-title">
                                    Your cart items
                                </h3>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="table-content table-responsive cart-table-content">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Product Name</th>
                                                        <th>Unit Price</th>
                                                        <th>Qty</th>
                                                        <th>Subtotal</th>
                                                        <th>action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {cartItems.map(
                                                        (cartItem, key) => {
                                                            const discountedPrice =
                                                                getDiscountPrice(
                                                                    cartItem.price,
                                                                    cartItem.discount
                                                                );
                                                            const finalProductPrice =
                                                                (
                                                                    cartItem.price *
                                                                    currency.currencyRate
                                                                ).toFixed(2);
                                                            const finalDiscountedPrice =
                                                                (
                                                                    discountedPrice *
                                                                    currency.currencyRate
                                                                ).toFixed(2);

                                                            discountedPrice !=
                                                            null
                                                                ? (cartTotalPrice +=
                                                                      finalDiscountedPrice *
                                                                      cartItem.quantity)
                                                                : (cartTotalPrice +=
                                                                      finalProductPrice *
                                                                      cartItem.quantity);
                                                            return (
                                                                <tr key={key}>
                                                                    <td className="product-thumbnail">
                                                                        <Link
                                                                            to={
                                                                                process
                                                                                    .env
                                                                                    .PUBLIC_URL +
                                                                                "/product/" +
                                                                                cartItem.id
                                                                            }
                                                                        >
                                                                            <img
                                                                                className="img-fluid"
                                                                                src={`https://source.unsplash.com/random/200x200?random=${key}`}
                                                                                alt=""
                                                                            />
                                                                        </Link>
                                                                    </td>

                                                                    <td className="product-name">
                                                                        <Link
                                                                            to={
                                                                                process
                                                                                    .env
                                                                                    .PUBLIC_URL +
                                                                                "/product/" +
                                                                                cartItem.id
                                                                            }
                                                                        >
                                                                            {
                                                                                cartItem.name
                                                                            }
                                                                        </Link>
                                                                        {cartItem.selectedProductColor &&
                                                                        cartItem.selectedProductSize ? (
                                                                            <div className="cart-item-variation">
                                                                                <span>
                                                                                    Color:{" "}
                                                                                    {
                                                                                        cartItem.selectedProductColor
                                                                                    }
                                                                                </span>
                                                                                <span>
                                                                                    Size:{" "}
                                                                                    {
                                                                                        cartItem.selectedProductSize
                                                                                    }
                                                                                </span>
                                                                            </div>
                                                                        ) : (
                                                                            ""
                                                                        )}
                                                                    </td>

                                                                    <td className="product-price-cart">
                                                                        {discountedPrice !==
                                                                        null ? (
                                                                            <>
                                                                                <span className="amount old">
                                                                                    {currency.currencySymbol +
                                                                                        finalProductPrice}
                                                                                </span>
                                                                                <span className="amount">
                                                                                    {currency.currencySymbol +
                                                                                        finalDiscountedPrice}
                                                                                </span>
                                                                            </>
                                                                        ) : (
                                                                            <span className="amount">
                                                                                {currency.currencySymbol +
                                                                                    finalProductPrice}
                                                                            </span>
                                                                        )}
                                                                    </td>

                                                                    <td className="product-quantity">
                                                                        <div className="cart-plus-minus">
                                                                            <button
                                                                                className="dec qtybutton"
                                                                                onClick={() =>
                                                                                    dispatch(
                                                                                        decreaseQuantityAsync(
                                                                                            cartItem
                                                                                        )
                                                                                    )
                                                                                }
                                                                            >
                                                                                -
                                                                            </button>
                                                                            <input
                                                                                className="cart-plus-minus-box"
                                                                                type="text"
                                                                                value={
                                                                                    cartItem.quantity
                                                                                }
                                                                                readOnly
                                                                            />
                                                                            <button
                                                                                className="inc qtybutton"
                                                                                onClick={() =>
                                                                                    dispatch(
                                                                                        addToCartAsync(
                                                                                            {
                                                                                                ...cartItem,
                                                                                                quantity:
                                                                                                    quantityCount,
                                                                                            }
                                                                                        )
                                                                                    )
                                                                                }
                                                                                disabled={
                                                                                    cartItem !==
                                                                                        undefined &&
                                                                                    cartItem.quantity &&
                                                                                    cartItem.quantity >=
                                                                                        cartItemStock(
                                                                                            cartItem,
                                                                                            cartItem.selectedProductColor,
                                                                                            cartItem.selectedProductSize
                                                                                        )
                                                                                }
                                                                            >
                                                                                +
                                                                            </button>
                                                                        </div>
                                                                    </td>
                                                                    <td className="product-subtotal">
                                                                        {discountedPrice !==
                                                                        null
                                                                            ? currency.currencySymbol +
                                                                              (
                                                                                  finalDiscountedPrice *
                                                                                  cartItem.quantity
                                                                              ).toFixed(
                                                                                  2
                                                                              )
                                                                            : currency.currencySymbol +
                                                                              (
                                                                                  finalProductPrice *
                                                                                  cartItem.quantity
                                                                              ).toFixed(
                                                                                  2
                                                                              )}
                                                                    </td>

                                                                    <td className="product-remove">
                                                                        <button
                                                                            onClick={() =>
                                                                                dispatch(
                                                                                    deleteFromCartAsync(
                                                                                        cartItem
                                                                                    )
                                                                                )
                                                                            }
                                                                        >
                                                                            <i className="fa fa-times"></i>
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            );
                                                        }
                                                    )}

                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <div className="fw-bold">
                                                                Grand Total
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="fw-bold">
                                                                {currency.currencySymbol +
                                                                    cartTotalPrice.toFixed(
                                                                        2
                                                                    )}
                                                            </div>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        <div className="cart-shiping-update-wrapper">
                                            <div className="cart-shipping-update">
                                                <Link
                                                    className="secondary-btn"
                                                    to={
                                                        process.env.PUBLIC_URL +
                                                        "/shop"
                                                    }
                                                >
                                                    Continue Shopping
                                                </Link>
                                            </div>
                                            <div className="grand-total">
                                                {
                                                    localStorage.getItem(
                                                        "loggedInUser"
                                                    ) ? (
                                                        <Link
                                                            className="primary-btn"
                                                            to={
                                                                process.env
                                                                    .PUBLIC_URL +
                                                                "/checkout"
                                                            }
                                                        >
                                                            Proceed to Checkout
                                                        </Link>
                                                    ) : (
                                                        <Link
                                                            className="primary-btn"
                                                            to={
                                                                process.env
                                                                    .PUBLIC_URL +
                                                                "/login?redirect=checkout"
                                                            }
                                                        >
                                                            Login to
                                                            Checkout
                                                        </Link>
                                                    )
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row align-items-end">
                                    {/* <div className="col-lg-6 col-md-6">
                                        <div className="discount-code-wrapper">
                                            <div className="title-wrap">
                                                <h4 className="cart-bottom-title section-bg-gray">
                                                    Use Coupon Code
                                                </h4>
                                            </div>
                                            <div className="discount-code">
                                                <p>
                                                    Enter your coupon code if
                                                    you have one.
                                                </p>
                                                <form>
                                                    <input
                                                        type="text"
                                                        required
                                                        name="name"
                                                    />
                                                    <button
                                                        className="cart-btn-2"
                                                        type="submit"
                                                    >
                                                        Apply Coupon
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                            </>
                        ) : (
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="item-empty-area text-center">
                                        <div className="item-empty-area__icon mb-30">
                                            <i className="pe-7s-cart"></i>
                                        </div>
                                        <div className="item-empty-area__text">
                                            No items found in cart <br />{" "}
                                            <Link
                                                to={
                                                    process.env.PUBLIC_URL +
                                                    "/shop"
                                                }
                                            >
                                                Shop Now
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </LayoutOne>
        </>
    );
};

export default Cart;
