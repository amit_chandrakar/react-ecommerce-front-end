import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import axios from 'axios';
import cogoToast from 'cogo-toast';
import { APP_URL } from "../../helpers/product";

const Login = () => {
    let { pathname } = useLocation();
    const [formData, setFormData] = useState({
        email: "",
        password: "",
        rememberMe: false,
    });

    // Get query string value
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const path = urlParams.get('redirect');

    // Login functionality
    const loginSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.post(`${APP_URL}/auth/login`, formData);
            // showToast('success', response.data.message);
            cogoToast.success(response.data.message, {position: "bottom-left"});

            localStorage.setItem('loginToken', response.data.data.token);
            localStorage.setItem('loggedInUser', JSON.stringify(response.data.data.user));

            setTimeout(() => {
                window.location.href = path ? process.env.PUBLIC_URL + path : process.env.PUBLIC_URL + "/";
            }, 1000);
        } catch (error) {
            // showToast('error', error.response.data.message); // Possible toasts => error, success, info, warning
            cogoToast.error(error.response.data.message, {position: "bottom-left"});
        }
    };

    useEffect(() => {
        if (localStorage.getItem('loginToken')) {
            window.location.href = process.env.PUBLIC_URL + "/";
        }
    }, []);

    return (
        <>
            <SEO
                titleTemplate="Login"
                description="Login page of flone react minimalist eCommerce template."
            />
            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Sign In",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />
                <div className="login-register-area pt-50 pb-70">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-7 col-md-12 ms-auto me-auto">
                                <h4 className="text-center mb-3 login-register-heading-text">Sign In</h4>
                                <div className="login-register-wrapper">
                                    <div className="login-form-container">
                                        <div className="login-register-form">
                                            <form onSubmit={loginSubmit}>
                                                <input
                                                    type="text"
                                                    name="email"
                                                    placeholder="Username"
                                                    value={formData.email}
                                                    onChange={(e) => setFormData({ ...formData, email: e.target.value })}
                                                />
                                                <input
                                                    type="password"
                                                    name="password"
                                                    placeholder="Password"
                                                    value={formData.password}
                                                    onChange={(e) => setFormData({ ...formData, password: e.target.value })}
                                                />
                                                <div className="button-box">
                                                    <div className="login-toggle-btn">
                                                        <input
                                                            type="checkbox"
                                                            id="remember_me"
                                                            name="rememberMe"
                                                            checked={formData.rememberMe}
                                                            onChange={(e) => setFormData({ ...formData, rememberMe: e.target.checked })}
                                                        />
                                                        <label
                                                            className="ml-10"
                                                            htmlFor="remember_me"
                                                        >
                                                            Remember me
                                                        </label>
                                                        <Link
                                                            to={
                                                                process.env
                                                                    .PUBLIC_URL +
                                                                "/"
                                                            }
                                                        >
                                                            Forgot Password?
                                                        </Link>
                                                    </div>
                                                    <button type="submit">
                                                        <span>Sign In</span>
                                                    </button>
                                                </div>

                                                <div className="text-center">
                                                    <p>
                                                        Don't have an account?{" "}
                                                        <Link
                                                            to={
                                                                process.env
                                                                    .PUBLIC_URL +
                                                                "/register"
                                                            }
                                                        >
                                                            Sign Up
                                                        </Link>
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutOne>
        </>
    );
};

export default Login;
