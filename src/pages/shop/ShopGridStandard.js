import { useState, useEffect } from "react";
import Paginator from "react-hooks-paginator";
import { useLocation } from "react-router-dom";
import { APP_URL, getSortedProducts } from "../../helpers/product";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import ShopSidebar from "../../wrappers/product/ShopSidebar";
import ShopTopbar from "../../wrappers/product/ShopTopbar";
import ShopProducts from "../../wrappers/product/ShopProducts";
import axios from "axios";

const ShopGridStandard = () => {
    const [layout, setLayout] = useState("grid three-column");
    const [sortType, setSortType] = useState("");
    const [sortValue, setSortValue] = useState("");
    const [searchText, setSearchText] = useState("");
    const [filterSortType, setFilterSortType] = useState("");
    const [filterSortValue, setFilterSortValue] = useState("");
    const [filterSearchText, setFilterSearchText] = useState("");
    const [offset, setOffset] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [currentData, setCurrentData] = useState([]);
    const [sortedProducts, setSortedProducts] = useState([]);
    const [products, setProducts] = useState([]);
    const [categories, setCategories] = useState([]);

    const pageLimit = 10;
    let { pathname } = useLocation();

    const getLayout = (layout) => {
        setLayout(layout);
    };

    const getSortParams = (sortType, sortValue) => {
        setSortType(sortType);
        setSortValue(sortValue);
    };

    const getFilterSortParams = (sortType, sortValue) => {
        setFilterSortType(sortType);
        setFilterSortValue(sortValue);
    };

    // Get search text
    const getSearchText = (text) => {
        setSearchText(text);
    };

    // GetCategories
    const getCategories = () => {
        axios
            .get(`${APP_URL}/get-products-and-its-category`)
            .then((res) => {
                setProducts(res.data);

                // Get unique categories
                const uniqueCategories = res.data.reduce((unique, item) => {
                    // Check if the category already exists in the unique array
                    if (
                        !unique.some(
                            (category) => category._id === item.category._id
                        )
                    ) {
                        unique.push(item.category); // Push the entire category object
                    }
                    return unique;
                }, []);

                setCategories(uniqueCategories);
            })
            .catch((err) => console.log(err));
    };

    useEffect(() => {
        getCategories();
    }, []);

    useEffect(() => {
        let sortedProducts = getSortedProducts(products, sortType, sortValue, searchText);
        const filterSortedProducts = getSortedProducts(
            sortedProducts,
            filterSortType,
            filterSortValue,
            filterSearchText
        );
        sortedProducts = filterSortedProducts;
        setSortedProducts(sortedProducts);
        setCurrentData(sortedProducts.slice(offset, offset + pageLimit));
    }, [
        offset,
        products,
        sortType,
        sortValue,
        searchText,
        filterSortType,
        filterSortValue,
        filterSearchText,
    ]);

    return (
        <>
            <SEO
                titleTemplate="Shop Page"
                description="Shop page of flone react minimalist eCommerce template."
            />

            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: "Home", path: process.env.PUBLIC_URL + "/" },
                        {
                            label: "Shop",
                            path: process.env.PUBLIC_URL + pathname,
                        },
                    ]}
                />

                <div className="shop-area pt-95 pb-100">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 order-2 order-lg-1">
                                {/* shop sidebar */}
                                <ShopSidebar
                                    categories={categories}
                                    products={products}
                                    getSortParams={getSortParams}
                                    getSearchText={getSearchText}
                                    sideSpaceClass="mr-30"
                                />
                            </div>
                            <div className="col-lg-9 order-1 order-lg-2">
                                {/* shop topbar default */}
                                <ShopTopbar
                                    getLayout={getLayout}
                                    getFilterSortParams={getFilterSortParams}
                                    productCount={products.length}
                                    sortedProductCount={currentData.length}
                                />

                                {/* shop page content default */}
                                <ShopProducts
                                    layout={layout}
                                    products={currentData}
                                />

                                {/* shop product pagination */}
                                <div className="pro-pagination-style text-center mt-30">
                                    <Paginator
                                        totalRecords={sortedProducts.length}
                                        pageLimit={pageLimit}
                                        pageNeighbours={2}
                                        setOffset={setOffset}
                                        currentPage={currentPage}
                                        setCurrentPage={setCurrentPage}
                                        pageContainerClass="mb-0 mt-0"
                                        pagePrevText="«"
                                        pageNextText="»"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutOne>
        </>
    );
};

export default ShopGridStandard;
