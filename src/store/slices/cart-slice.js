import { v4 as uuidv4 } from "uuid";
import cogoToast from "cogo-toast";
import axios from "axios";
import { APP_URL } from "../../helpers/product";
const { createSlice } = require("@reduxjs/toolkit");

// Get session id
const sessionId = localStorage.getItem("sessionId");

const cartSlice = createSlice({
    name: "cart",
    initialState: {
        cartItems: [],
    },
    reducers: {
        addToCart(state, action) {
            const product = action.payload;

            const cartItem = state.cartItems.find(
                (item) => item._id === product._id
            );

            if (!cartItem) {
                // The cart is empty, add the product
                state.cartItems.push({
                    ...product,
                    quantity: product.quantity ? product.quantity : 1,
                    cartItemId: uuidv4(),
                });
            } else {
                // Search for the product in the cart
                const cartItem = state.cartItems.find(
                    (item) => item._id === product._id
                );

                if (cartItem === undefined) {
                    // The product is not in the cart, add it
                    state.cartItems = [
                        ...state.cartItems,
                        {
                            ...product,
                            quantity: product.quantity ? product.quantity : 1,
                            cartItemId: uuidv4(),
                        },
                    ];
                } else {
                    // The product is already in the cart, update its quantity
                    state.cartItems = state.cartItems.map((item) => {
                        if (item.cartItemId === cartItem.cartItemId) {
                            return {
                                ...item,
                                quantity: product.quantity
                                    ? item.quantity + product.quantity
                                    : item.quantity + 1,
                            };
                        }
                        return item;
                    });
                }
            }

            cogoToast.success("Added To Cart", { position: "bottom-left" });
        },
        deleteFromCart(state, action) {
            state.cartItems = state.cartItems.filter(
                (item) => item.cartItemId !== action.payload
            );
            cogoToast.error("Removed From Cart", { position: "bottom-left" });
        },
        decreaseQuantity(state, action) {
            const product = action.payload;
            if (product.quantity === 1) {
                state.cartItems = state.cartItems.filter(
                    (item) => item.cartItemId !== product.cartItemId
                );
                cogoToast.error("Removed From Cart", {
                    position: "bottom-left",
                });
            } else {
                state.cartItems = state.cartItems.map((item) =>
                    item.cartItemId === product.cartItemId
                        ? { ...item, quantity: item.quantity - 1 }
                        : item
                );
                cogoToast.warn("Item Decremented From Cart", {
                    position: "bottom-left",
                });
            }
        },
        deleteAllFromCart(state) {
            state.cartItems = [];
        },
    },
});

// Asynchronous action creator to add item to cart
export const addToCartAsync = (product) => async (dispatch) => {
    dispatch(addToCart(product));

    // Get userId from local storage
    const userId =
        localStorage.getItem("loggedInUser") &&
        JSON.parse(localStorage.getItem("loggedInUser"))._id;
    const quantity = product.quantity ? product.quantity : 1;

    // Add product to cart via axios request
    axios
        .post(
            `${APP_URL}/carts/add-to-cart/${sessionId}`,
            {
                product,
                sessionId,
                userId,
                quantity,
            }
        )
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log(error);
        });
};

// Asynchronous action creator to delete item from cart
export const deleteFromCartAsync = (item) => async (dispatch) => {
    dispatch(deleteFromCart(item.cartItemId));

    // Add product to cart via axios request
    axios
        .delete(
            `${APP_URL}/carts/remove-from-cart/${sessionId}`,
            {
                params: {
                    productId: item._id,
                },
            }
        )
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log(error);
        });
};

// Asynchronous action creator to decrease item quantity from cart
export const decreaseQuantityAsync = (product) => async (dispatch) => {
    dispatch(decreaseQuantity(product));

    // Add product to cart via axios request
    axios
        .post(
            `${APP_URL}/carts/decrement-quantity/${sessionId}`,
            {
                productId: product._id
            }
        )
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log(error);
        });

};

// Asynchronous action creator to delete all items from cart
export const deleteAllFromCartAsync = () => async (dispatch) => {
    dispatch(deleteAllFromCart());
};

export const {
    addToCart,
    deleteFromCart,
    decreaseQuantity,
    deleteAllFromCart,
} = cartSlice.actions;
export default cartSlice.reducer;
