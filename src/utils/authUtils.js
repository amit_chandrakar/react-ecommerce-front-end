import { jwtDecode } from 'jwt-decode';

export const checkAuthTokenValidity = () => {
    const jwtToken = localStorage.getItem('loginToken');

    if (jwtToken) {
        try {
            const decodedToken = jwtDecode(jwtToken);
            const currentTime = Date.now() / 1000;
            return decodedToken.exp > currentTime;
        } catch (error) {
            console.error('Invalid JWT token:', error);
            return false; // Token decoding failed
        }
    }

    return false; // No token found
};
