const ShopSearch = ({ getSearchText }) => {
  return (
    <div className="sidebar-widget">
      <h4 className="pro-sidebar-title">Search </h4>
      <div className="pro-sidebar-search mb-50 mt-25">
        <form className="pro-sidebar-search-form" onSubmit={(event) => event.preventDefault() }>
          <input
            type="text"
            placeholder="Search here..."
            onChange={e => {
              getSearchText(e.target.value);
            }}
          />
          <button type="button">
            <i className="pe-7s-search" />
          </button>
        </form>
      </div>
    </div>
  );
};

export default ShopSearch;
