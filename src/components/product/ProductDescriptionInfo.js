import PropTypes from "prop-types";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getProductCartQuantity } from "../../helpers/product";
import Rating from "./sub-components/ProductRating";
import { addToCartAsync } from "../../store/slices/cart-slice";
import { addToWishlist } from "../../store/slices/wishlist-slice";
import { addToCompare } from "../../store/slices/compare-slice";

const ProductDescriptionInfo = ({
    product,
    discountedPrice,
    currency,
    finalDiscountedPrice,
    finalProductPrice,
    cartItems,
    wishlistItem,
    compareItem,
}) => {
    const dispatch = useDispatch();
    const [quantityCount, setQuantityCount] = useState(1);
    const productStock = product.stock;
    const productCartQty = getProductCartQuantity(cartItems, product);

    return (
        <div className="product-details-content ml-70">
            <h2>{product.name}</h2>

            <div className="product-details-price">
                {discountedPrice !== null ? (
                    <>
                        <span>
                            {currency.currencySymbol + finalDiscountedPrice}
                        </span>{" "}
                        <span className="old">
                            {currency.currencySymbol + finalProductPrice}
                        </span>
                    </>
                ) : (
                    <span>{currency.currencySymbol + finalProductPrice} </span>
                )}
            </div>

            {product.rating && product.rating > 0 ? (
                <div className="pro-details-rating-wrap">
                    <div className="pro-details-rating">
                        <Rating ratingValue={product.rating} />
                    </div>
                </div>
            ) : (
                ""
            )}

            <div className="pro-details-list">
                <p>{product.shortDescription}</p>
            </div>

            {product.affiliateLink ? (
                <div className="pro-details-quality">
                    <div className="pro-details-cart btn-hover ml-0">
                        <a
                            href={product.affiliateLink}
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            Buy Now
                        </a>
                    </div>
                </div>
            ) : (
                <div className="pro-details-quality">
                    <div className="cart-plus-minus">
                        <button
                            onClick={() =>
                                setQuantityCount(
                                    quantityCount > 1 ? quantityCount - 1 : 1
                                )
                            }
                            className="dec qtybutton"
                        >
                            -
                        </button>
                        <input
                            className="cart-plus-minus-box"
                            type="text"
                            value={quantityCount}
                            readOnly
                        />
                        <button
                            onClick={() =>
                                setQuantityCount(
                                    quantityCount <
                                        productStock - productCartQty
                                        ? quantityCount + 1
                                        : quantityCount
                                )
                            }
                            className="inc qtybutton"
                        >
                            +
                        </button>
                    </div>
                    <div className="pro-details-cart btn-hover">
                        {productStock && productStock > 0 ? (
                            <button
                                onClick={() =>
                                    dispatch(
                                        addToCartAsync({
                                            ...product,
                                            quantity: quantityCount,
                                        })
                                    )
                                }
                                disabled={productCartQty >= productStock}
                            >
                                {" "}
                                Add To Cart{" "}
                            </button>
                        ) : (
                            <button disabled>Out of Stock</button>
                        )}
                    </div>
                    <div className="pro-details-wishlist">
                        <button
                            className={
                                wishlistItem !== undefined ? "active" : ""
                            }
                            disabled={wishlistItem !== undefined}
                            title={
                                wishlistItem !== undefined
                                    ? "Added to wishlist"
                                    : "Add to wishlist"
                            }
                            onClick={() => dispatch(addToWishlist(product))}
                        >
                            <i className="pe-7s-like" />
                        </button>
                    </div>
                    <div className="pro-details-compare">
                        <button
                            className={
                                compareItem !== undefined ? "active" : ""
                            }
                            disabled={compareItem !== undefined}
                            title={
                                compareItem !== undefined
                                    ? "Added to compare"
                                    : "Add to compare"
                            }
                            onClick={() => dispatch(addToCompare(product))}
                        >
                            <i className="pe-7s-shuffle" />
                        </button>
                    </div>
                </div>
            )}

            {product.category ? (
              <>
                <div className="pro-details-meta">
                    <span>Category :</span>
                    <ul>
                        <li>
                            <Link to={process.env.PUBLIC_URL + "/shop"}>
                                {product.category.name}
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="pro-details-meta">
                    <span>Brand :</span>
                    <ul>
                        <li>
                            <Link to={process.env.PUBLIC_URL + "/shop"}>
                                {product.brand.name}
                            </Link>
                        </li>
                    </ul>
                </div>
              </>
            ) : (
                ""
            )}

            {product.tags ? (
                <div className="pro-details-meta">
                    <span>Tags :</span>
                    <ul>
                        {product.tags.map((single, key) => {
                            return (
                                <li key={key}>
                                    <Link to={process.env.PUBLIC_URL + "/shop"}>
                                        {single}
                                    </Link>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            ) : (
                ""
            )}

            <div className="pro-details-social">
                <ul>
                    <li>
                        <a href="//facebook.com">
                            <i className="fa fa-facebook" />
                        </a>
                    </li>
                    <li>
                        <a href="//dribbble.com">
                            <i className="fa fa-dribbble" />
                        </a>
                    </li>
                    <li>
                        <a href="//pinterest.com">
                            <i className="fa fa-pinterest-p" />
                        </a>
                    </li>
                    <li>
                        <a href="//twitter.com">
                            <i className="fa fa-twitter" />
                        </a>
                    </li>
                    <li>
                        <a href="//linkedin.com">
                            <i className="fa fa-linkedin" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

ProductDescriptionInfo.propTypes = {
    cartItems: PropTypes.array,
    compareItem: PropTypes.shape({}),
    currency: PropTypes.shape({}),
    discountedPrice: PropTypes.number,
    finalDiscountedPrice: PropTypes.number,
    finalProductPrice: PropTypes.number,
    product: PropTypes.shape({}),
    wishlistItem: PropTypes.shape({}),
};

export default ProductDescriptionInfo;
