import axios from "axios";
import PropTypes from "prop-types";
import { useState } from "react";
import cogoToast from "cogo-toast";
import { APP_URL } from "../../../helpers/product";

const CustomForm = () => {

  const [error, setError] = useState(null);

  let email;
  const submit = async () => {
    // Validate email
    if (email.value === "") {
      setError("Please enter a valid email address");
      return false;
    }
    // Validate email with regex
    const regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    if (!regex.test(email.value)) {
      setError("Please enter a valid email address");
      return false;
    }

    // Send data to server
    try {
      const response = await axios.post(`${APP_URL}/subscribe`, {
        email: email.value
      });

      if (response.data.status === "success") {
        cogoToast.success(response.data.message , { position: "bottom-left" });

        // Clear input field
        email.value = "";
      } else {
        cogoToast.error(response.data.message , { position: "bottom-left" });
      }
  } catch (error) {
      cogoToast.error("Something went wrong. Please try again.", { position: "bottom-left" });
  }
  };

  return (
    <div className="subscribe-form">
      <div className="mc-form">
        <div>
          <input
            id="mc-form-email"
            className="email"
            ref={node => (email = node)}
            type="email"
            placeholder="Enter your email address..."
          />
        </div>
        <div className="clear">
          <button type="button" className="button" onClick={submit}>
            SUBSCRIBE
          </button>
        </div>
      </div>
      {error && (
        <div
          style={{ color: "#e74c3c", fontSize: "12px" }}
          dangerouslySetInnerHTML={{ __html: error }}
        />
      )}
    </div>
  );
};

const SubscribeEmail = () => {
  return (
    <div>
      <CustomForm />
    </div>
  );
};

SubscribeEmail.propTypes = {
  mailchimpUrl: PropTypes.string
};

export default SubscribeEmail;
